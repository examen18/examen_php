<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Examen PHP</title>
    </head>
    <body>
        <?php
            $cont=0;
            $textoRep = 'No hay elementos repetidos';
            $array = $_GET['valores'];
            $arrayRep = array_count_values($array);
            foreach($arrayRep as $x=>$val){
                if($val>1){
                    $textoRep = 'Hay elementos repetidos';
                }
            }
            $cajasRell = count($array);
            foreach($array as $valor){
                if($valor!= ""){
                    $cont++;
                }
            }
        ?>
        
        <h1>EJERCICIO NUMERO 1 del EXAMEN DE PHP</h1>
        
        <div>
            <p><?=$textoRep?></p>
            <p>Se han rellenado <?= $cont ?> cajas de un total de <?= $cajasRell ?></p>
        </div>
        
    </body>
</html>
