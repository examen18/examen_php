<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Examen PHP</title>
    </head>
    <body>
        <h1>EJERCICIO NUMERO 1 del EXAMEN DE PHP</h1>
        <p>Escriba un número (0 < número <= 10) y dibujaré una tabla de una columna de ese tamaño con cajas de texto en cada celda.</p>
        
        <div>
            <h3>Ejercicio 1</h3> 
            <form method="get" action="index_2.php">
              <label for="numero">Tamaño de la tabla</label>  
              <input type="number" name="numero" id="numero" placeholder="Introduce un número"/>
              <button name="dibujar">Dibujar</button>
              <input type="reset" value="Restablecer" name="reset" />
            </form>
        </div>

    </body>
</html>
